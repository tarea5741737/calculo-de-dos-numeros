function Resultado() {
    var n1 = parseInt(document.getElementById("n1").value);
    var n2 = parseInt(document.getElementById("n2").value);

    // Verifica si n1 o n2 no son números válidos
    if (isNaN(n1) || isNaN(n2)) {
        alert("Error: Por favor, ingrese números válidos antes de calcular.");
        return; // Detiene la ejecución de la función
    }

    var total;

    // Borra el contenido anterior
    document.getElementById("total").innerHTML = "";

    for (var i = 0; i < 5; i++) {
        switch (i) {
            case 0:
                total = n1 + n2;
                document.getElementById("total").innerHTML += " El resultado de la suma es: " + total + "<br>";
                break;
            case 1:
                total = n1 - n2;
                document.getElementById("total").innerHTML += " El resultado de la resta es: " + total + "<br>";
                break;
            case 2:
                total = n1 * n2;
                document.getElementById("total").innerHTML += " El resultado de la multiplicación es: " + total + "<br>";
                break;
            case 3:
                total = n1 / n2;
                document.getElementById("total").innerHTML += " El resultado de la división es: " + total + "<br>";
                break;
            case 4:
                total = n1 % n2;
                document.getElementById("total").innerHTML += " El resultado de MOD es: " + total + "<br>";
                break;
            default:
                break;
        }
    }
}
